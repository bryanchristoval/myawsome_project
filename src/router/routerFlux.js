import React, { Component } from 'react'
import { Container } from 'native-base'
import { Router, Scene } from 'react-native-router-flux'
import TabNavigator from './router';
import AppDetail from '../components/screen/detail'
import PersonDetail from '../components/screen/persondetail'
import AddPerson from '../components/screen/addperson'



class RouterFlux extends Component {
    render () {
        return (
            <Container>
                <Router>
                    <Scene key='root'>
                        <Scene key='index' hideNavBar component={TabNavigator}/>
                        <Scene key='detailpage' hideNavBar={true} component={AppDetail}/>
                        <Scene key='persondetail' hideNavBar component={PersonDetail}/>
                        <Scene key='addperson' hideNavBar component={AddPerson}/>
                    </Scene>
                </Router>
            </Container>
        )
    }
}

export default RouterFlux