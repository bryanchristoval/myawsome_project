import React, { Component } from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import AppMain from '../components/screen/main'
import AppProduct from '../components/screen/product'
import AppData from '../components/screen/data'
import AppProfile from '../components/screen/profile'
import {Header, Left, Icon, Button, Title, Body, Right, Container} from 'native-base'
const TabNavigator = createBottomTabNavigator({

    Home: AppMain,
    Product: AppProduct,
    Messages: AppData,
    Profile: AppProfile,
    
    

}, {
  tabBarPosition: 'bottom',
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: '#f2f2f2',
    activeBackgroundColor: "#F08080",
    inactiveTintColor: '#666',
    labelStyle: {
      fontSize: 18,
      padding: 12
    }
  }
});

const RouterButton = createAppContainer(TabNavigator);

class AppTab extends Component {
  render(){
    return(
      <Container>
        <RouterButton/>
      </Container>
    )
  }
}

export default AppTab;