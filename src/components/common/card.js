import React, { Component } from 'react'
import {View, Text, Image, StyleSheet, Dimensions} from 'react-native'

class Card extends Component {
    render () {
        return (
            <View>
                <View style={styles.Card}>
                    
                    <Image
                        style={styles.ImageStyle}
                        source={{uri: this.props.productimage}}
                    />
                    <Text style={styles.ProductTitle} key={this.props.key}>{this.props.producttitle}</Text>
                    <Text key={this.props.key}>{this.props.productprice}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    ContentTitle:{
        fontSize: 30,
        fontWeight: 'bold',
    },
    Content:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        flexWrap: 'wrap',
        
        
    },
    Card:{
        width: 205,
        height: 280,
        backgroundColor: 'white',
        //margin: 7,
        alignItems: 'center',
        borderColor: '#f2f2f2',
        //borderRadius: 5,
        borderWidth: 1,
    
    },
    ProductTitle:{
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginTop:5,
        marginBottom:8,
    },
    ImageStyle:{
        width: 150, 
        height: 180,
        marginBottom: 15,
        marginTop: 10,

    }
})

export default Card