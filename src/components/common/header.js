import React, { Component } from 'react'
import {Header, Left, Icon, Button, Title, Body, Right} from 'native-base'

class AppHeader extends Component {
    render () {
        return (
            <Header>
            <Left>
              <Button transparent onPress={this.props.buttonleft}>
                <Icon
                  name = {this.props.sidebaricon}
                />
              </Button>
            </Left>
            <Body>
              <Title>
                {this.props.title}
              </Title>
            </Body>
            <Right>
              <Button transparent onPress={this.props.buttonright}>
                <Icon
                  name = {this.props.settingbutton}
                />
              </Button>
            </Right>
          </Header>
        )
    }
}

export default AppHeader