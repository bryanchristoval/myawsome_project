import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container } from 'native-base';
import AppHeader from '../common/header';
import { Actions } from 'react-native-router-flux'

class AppDetail extends Component {
    render () {
        return (
            <Container>
                <AppHeader
                    sidebaricon ='arrow-back'
                    buttonleft={() => Actions.pop()}      
                />
            </Container>
        )
    }
}

export default AppDetail