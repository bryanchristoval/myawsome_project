import React, { Component } from 'react'
import { Container, Content, Form, Label, Input, Item, Button, Text} from 'native-base'
import AppHeader from '../common/header'
import { Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import axios from 'axios'

class AddPerson extends Component {
    constructor(props){
        super(props);
        this.state = {
            title:'',
            categories: '',
            content:'',

        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    

    handleSubmit = event => {
        event.preventDefault()
        axios.post('http://reduxblog.herokuapp.com/api/posts?key=bryan1234',
            {title: this.state.title, categories: this.state.categories, content: this.state.content}
        )
        .then(response => {
            console.log(response.data)
            Alert.alert(
                'Alert Title',
                'My Alert Msg',
                [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => Actions.index()},
                ],
                {cancelable: false},
              );
        })
        .catch(error => {
            console.log(error)
        })
    }

    render () {
        return (
            <Container>
                <AppHeader
                    sidebaricon='ios-arrow-back'
                    buttonleft = {() => Actions.pop()}
                    title ='Add Data'
                />
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Label> title</Label>
                            <Input onChangeText={(title) => this.setState({title})}/>
                        </Item>
                        <Item floatingLabel>
                            <Label> Image Link</Label>
                            <Input onChangeText={(categories) => this.setState({categories})}/>
                        </Item>
                        <Item floatingLabel last>
                            <Label> Content</Label>
                            <Input onChangeText={(content) => this.setState({content})}/>
                        </Item>
                    </Form>
                    <Button onPress={this.handleSubmit}><Text>SUBMIT</Text></Button>
                    
                </Content>
            </Container>
            
        )
    }
}

export default AddPerson