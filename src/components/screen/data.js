import React, { Component } from 'react'
import { StyleSheet, View} from 'react-native'
import {Container, Content,Text, Right, Body, Title, Spinner, Thumbnail, ListItem, Icon, Button, List, Left, Form, Picker} from 'native-base'
import AppHeader from '../common/header';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';

class AppData extends Component {

  constructor(props){
    super(props);
    this.state ={
      datas: [],
      spin: true,
      selected: 'bryan1234'
    }
    this.onSendSelected = this.onSendSelected.bind(this)
  }


  onValueChange(value : String){
    this.setState({
      selected : value
    });

  }

  onSendSelected(){
  
    axios.get(`http://reduxblog.herokuapp.com/api/posts?key=${this.state.selected}`)
      .then(response =>{
        this.setState({
          datas : response.data,
          spin: false
        })
        console.log('data from postman',response.data)
        
      })
      .catch(err =>{
          console.log(err)})
  }
    
  

  componentDidMount(){
    
    axios.get(`http://reduxblog.herokuapp.com/api/posts?key=${this.state.selected}`)
      .then(response =>{
        this.setState({
          datas : response.data,
          spin: false
        })
        console.log('data from postman',response.data)
        
      })
      .catch(err =>{
          console.log(err)})
  }


  
  render () {


    
    let spinView
    if(this.state.spin === true){
      spinView = <Spinner color='black'/>
    }else if (this.state.datas.length !== 0){
      spinView = <List>
      {
        this.state.datas.map((data, index)=> 
          <ListItem avatar key={index} button onPress ={() => Actions.persondetail({ detail : data })}>
            <Left>
              <Thumbnail source={{ uri: data.categories }} />
            </Left>
            <Body>
              <Text style={styles.textName}>{data.title}</Text>
              <Text>{data.content}</Text>
            </Body>
            <Right>
              <Text note>3:43 pm</Text>
            </Right>
          </ListItem>
        
        )
      }
    </List>
    } else if(this.state.datas.length === 0){
      spinView = <Text>Sorry, your data is empty.</Text>
    }
    return (
      <Container>      
          <AppHeader buttonright ={() => Actions.addperson()}
            sidebaricon ='ios-menu'
            title = 'CHAT ROOM'
            settingbutton  = 'ios-add'
          />
          <Content>
          <View style={{flexDirection: 'row'}}>
            <Form style={{width: 300}}>
              <Picker
                mode="dropdown"
                iosHeader="Select your SIM"
                iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                style={{ width: undefined }}
                selectedValue={this.state.selected}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Picker.Item label="1" value="bryan1234" />
                <Picker.Item label="2" value="2" />
              </Picker>
            </Form>
            <Button onPress={this.onSendSelected}><Text>Send</Text></Button>
          </View>
            {spinView}
          
          </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({

textName:{
  fontWeight: 'bold',
}

})


export default AppData