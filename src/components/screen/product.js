import React, { Component } from 'react'
import {Container} from 'native-base'
import AppHeader from '../common/header';
import { Header, Tab, Tabs, TabHeading, Icon, Text, Content } from 'native-base';
import Tab1 from '../productscreen/tabs1'
import Tab2 from '../productscreen/tabs2'

class AppProduct extends Component {
    render () {
        return (
            <Container>
                <AppHeader
                    sidebaricon ='ios-menu'
                    title = 'PRODUCTS'
                />
                <Tabs>
                    <Tab heading={ <TabHeading><Text>Male</Text></TabHeading>}>
                        <Content>
                            <Tab1 />
                        </Content>
                    </Tab>
                    <Tab heading={ <TabHeading><Text>Female</Text></TabHeading>}>
                        <Content>
                            <Tab2 />
                        </Content>
                    </Tab>
                </Tabs>
            </Container>
        )
    }
}

export default AppProduct