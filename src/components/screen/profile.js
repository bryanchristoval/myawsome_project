import React, { Component } from 'react'
import {Container, Content,Text, Right, Body, Title, Icon, Grid, Button, Item, Input } from 'native-base'
import { Image, View, StyleSheet, TouchableOpacity } from 'react-native'
import AppHeader from '../common/header'
//import { Actions } from 'react-native-router-flux'
import ImagePicker from 'react-native-image-picker'
import { Col } from 'react-native-easy-grid';

import firebaseService from '../../firebase/firebase';
import RNFetchBlob from 'react-native-fetch-blob';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class AppProfile extends Component {
  constructor(props){
    super(props);
    this.state={
      nophotourl: 'https://proseawards.com/wp-content/uploads/2015/08/no-profile-pic.png'
    }

    this.getImage = this.getImage.bind(this)
  }
uploadImage(uri, mime = 'application/octet-stream'){
  return new Promise((resolve, reject) => {
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri

    const imageRef = firebaseService.storage().ref('images').child('image_001')

    fs.readFile(uploadUri, 'base64')
    .then((data) => {
      return Blob.build(data, {type: `${mime};BASE64`})
    })
    .then((blob) => {
      uploadBlob = blob
      return imageRef.put(blob, {contentType: mime})
    })
    .then(() => {
      uploadBlob.close()
      return imageRef.getDownloadURL()
    })
    .then((url) => {
      resolve(url)
    })
    .catch((error) => {
      reject(error)
    })  
  })
}
  

  getImage(){
    const options = {
      quality: 1.0,
      maxwidth: 500,
      maxheight: 500,
      storageoptions: {
        skipBackup: true,
      }
    };


  
    ImagePicker.showImagePicker(options, (response) => {
      console.log('RESPONE', response)
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } 
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } 
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } 
      else {
        this.uploadImage(response.uri)
        .then(uri => {alert('upload'); this.setState({nophotourl: url})})
        .catch(error => console.log(error))
      }
    });
  }

  render () {
    return (
      <Container>
          <AppHeader
            sidebaricon ='ios-menu'
            title = 'Profile'
            settingbutton= 'Done'
          />
          <Content>
              <TouchableOpacity onPress={this.getImage}>
                <View style={styles.container}>
                    <Image style={styles.avatar} source={{ uri: this.state.nophotourl}}/>
                </View> 
              </TouchableOpacity>
              <Grid style={{marginTop: 30}}>
                <Col style={styles.grid}>
                  <Text>Username</Text>
                </Col>
                <Col>
                  <Item>
                    <Input placeholder="Username" />
                  </Item>
                </Col>
              </Grid>
              <Grid>
                <Col style={styles.grid}>
                  <Text>Email</Text>
                </Col>
                <Col>
                  <Item>
                    <Input placeholder="Email" />
                  </Item>
                </Col>
              </Grid>
              <Grid>
                <Col style={styles.grid}>
                  <Text>Phone</Text>
                </Col>
                <Col>
                  <Item>
                    <Input placeholder="Phone Number" />
                  </Item>
                </Col>
              </Grid>
          </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
    

  },
  avatar:{
    margin: 10,
    height: 150,
    width: 150,
    borderRadius: 75,

  },
  grid:{
    width: 110,
    marginTop: 14,
    marginLeft: 10,
  }

})

export default AppProfile