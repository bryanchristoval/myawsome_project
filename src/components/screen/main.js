import React, { Component } from 'react'
import {StyleSheet} from 'react-native'
import {Container, Content, Header, Text, Item, Body, Title, Icon, Button, Input} from 'native-base'
import AppHeader from '../common/header'
import TopView from '../homescreen/topview'
import Swiper from '../homescreen/swiper'
import Card from '../common/card'

class AppMain extends Component {
  state ={
    cardDatas:[
    {    
        id: '01',
        name:'Nike',
        image:'https://cdn-images.farfetch-contents.com/12/96/03/43/12960343_13503179_480.jpg',
        price: '5.000.000'
    },
    {
        id: '08',
        name:'Moncler',
        image:'https://cdn-images.farfetch-contents.com/13/57/13/22/13571322_16105917_300.jpg',
        price: '5.000.000'
        
    },
    ]}
    
  render () {
    return (
      <Container>
          <AppHeader
            sidebaricon ='ios-menu'
            title = 'HOME'
            settingbutton = 'md-settings'
          />
          <Header searchBar rounded>
              <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" />
                <Icon name="ios-people" />
              </Item> 
              <Button transparent>
                <Text>Search</Text>
              </Button>         
          </Header>
          
          <Content>
            
              <TopView/>
              <Swiper/>
              <Item style={StyleSheet.cardStyle}>
                <Card />
                <Card />
                <Card />
                <Card />
              </Item>
              
              
            
          </Content>


            
            
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  cardStyle:{
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent:'center',
      
  }
})

export default AppMain