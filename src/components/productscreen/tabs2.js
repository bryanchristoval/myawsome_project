import React, { Component } from 'react'
import {View, Text} from 'react-native'
import {Header, Item, Input, Icon, Button, Grid, Col} from 'native-base';
import Card from '../common/card'

class Tab2 extends Component {
    state ={
        cardDatas:[
        {    
            id: '01',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        
        {
            id: '02',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '03',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '04',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '05',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '06',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '07',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
        {
            id: '08',
            name:'Apple',
            image:'https://auto.ndtvimg.com/bike-images/colors/ktm/rc-200/ktm-rc-200-white.png?v=12',
            price: '5.000.000'
        },
    ]}
    
    render () {
        const {cardDatas} = this.state
        return (
            <View>
                <Grid>
                    <Col>
                        <View>
                        {cardDatas.map((cardData, index)=>
                            <View key={index}>
                                <Card
                                    productimage = {cardData.image}
                                    producttitle = {cardData.name}
                                />
                            </View>
                            )}
                
                        </View>
                    </Col>
                    <Col>
                        <View>
                        {cardDatas.map((cardData, index)=>
                            <View key={index}>
                                <Card
                                    productimage = {cardData.image}
                                    producttitle = {cardData.name}
                                />
                            </View>
                            )}
                
                        </View>
                    </Col>
                </Grid>
                
            </View>
            
        )
    }
}

export default Tab2