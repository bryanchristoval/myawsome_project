import React, { Component } from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {Header, Item, Input, Icon, Button, Grid, Col} from 'native-base';
import Card from '../common/card'

class Tab1 extends Component {
    state ={
        cardDatas:[
        {    
            id: '01',
            name:'Nike',
            image:'https://cdn-images.farfetch-contents.com/12/96/03/43/12960343_13503179_480.jpg',
            price: '5.000.000'
        },
        
        {
            id: '02',
            name:'Adidas',
            image:'https://cdn-images.farfetch-contents.com/12/43/12/89/12431289_11433072_480.jpg',
            price: '5.000.000'
        },
        {
            id: '03',
            name:'Saint Laurent',
            image:'https://cdn-images.farfetch-contents.com/13/41/90/66/13419066_15694971_300.jpg',
            price: '5.000.000'
        },
        {
            id: '04',
            name:'Common Project',
            image:'https://cdn-images.farfetch-contents.com/12/45/17/14/12451714_11660068_480.jpg',
            price: '5.000.000'
        },
        {
            id: '05',
            name:'Acne Studios',
            image:'https://cdn-images.farfetch-contents.com/13/50/46/09/13504609_17051882_300.jpg',
            price: '5.000.000'
        },
        {
            id: '06',
            name:'Balenciaga',
            image:'https://cdn-images.farfetch-contents.com/12/49/15/34/12491534_12214285_480.jpg',
            price: '5.000.000'
        },
        {
            id: '07',
            name:'Givenchy',
            image:'https://cdn-images.farfetch-contents.com/13/66/22/63/13662263_16461449_300.jpg',
            price: '5.000.000'
        },
        {
            id: '08',
            name:'Moncler',
            image:'https://cdn-images.farfetch-contents.com/13/57/13/22/13571322_16105917_300.jpg',
            price: '5.000.000'
        },
    ]}
    
    render () {
        const {cardDatas} = this.state
        return (
            <View>
                <Grid>
                    <Col>
                        <View style = {styles.cardStyle}>
                        {cardDatas.map((cardData, index)=>
                            <View key={index}>
                                <Card
                                    producttitle = {cardData.name}
                                    productimage = {cardData.image}
                                    productprice = {cardData.price}
                                    
                                />
                            </View>
                            )}
                
                        </View>
                    </Col>
                </Grid>
                
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    cardStyle:{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent:'center',
        
    }
})

export default Tab1