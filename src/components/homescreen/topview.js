import React, { Component } from 'react';
import {View, Text, StyleSheet, Image} from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import {Header, Left, Icon, Button, Title, Body, Right} from 'native-base'
 class TopView extends Component {
  render() {
    return (
        <View style={styles.topView}>
            <Grid>
                <Col style={styles.col}>
                    <View style={styles.icon}>
                        <Icon style={{fontSize: 50,}}
                           name='ios-card'
                        />
                    </View>
                    <Text style={styles.textstyle}>
                        Top up
                    </Text>
                </Col>
                <Col style={styles.col}>
                    <View style={styles.icon}>
                        <Image
                            style={{width: 38, height: 38}}
                            source={require('../images/nearby.png')}
                        />
                    </View>
                    <Text style={styles.textstyle}>
                        Nearby
                    </Text>
                </Col>
                <Col style={styles.col}>
                    <View style={styles.icon}>
                        <Image
                                style={{width: 40, height: 40}}
                                source={require('../images/financewallet.png')}
                            />
                    </View>
                    <Text style={styles.textstyle}>
                        Wallet
                    </Text>
                </Col>
                <Col style={styles.col}>
                    <View style={styles.icon}>
                        <Icon style={{fontSize: 50,}}
                            name='ios-add'
                        />
                    </View>
                    <Text style={styles.textstyle}>
                        More
                    </Text>
                </Col>
                
            </Grid>
            
            
        </View>
        
    )
  }
}

const styles = StyleSheet.create({
    topView:{
        backgroundColor: 'salmon',
        height: 130,
        margin: 8,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        
    },

    col:{
        fontSize: 16,
        color:'white',
        alignItems:'center',
        justifyContent:'center',
    },
    
    icon:{
        borderRadius: 75/2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        height: 75,
        width: 75,
        margin: 10,

    },
    textstyle:{
        color: 'white',
        fontSize: 17,
    },
    titletopview:{
        backgroundColor: 'red',
        width: 900,
        
    }
})
export default TopView;