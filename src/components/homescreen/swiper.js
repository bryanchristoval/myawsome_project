import React, { Component } from 'react';
import { Text, Dimensions, Image, StyleSheet, View } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';

class Swiper extends Component {
    render () {
        return (
            <View style={styles.container}>
        <SwiperFlatList
          // autoplay
          // autoplayDelay={4}
          //autoplayLoop
          index={2}
          showPagination
        >
          <View style={styles.child}>
            <Image 
              source={require('../images/swiper1.jpeg')}
              style={styles.child}
            />
          </View>
          <View style={styles.child}>
            <Image source={require('../images/swiper2.jpg')}  style={styles.child}/>
          </View>
          <View style={[styles.child, { backgroundColor: 'skyblue' }]}>
            <Image source={require('../images/swiper3.jpg')}  style={styles.child}/>
          </View>
          <View style={[styles.child, { backgroundColor: 'teal' }]}>
            <Image source={require('../images/swiper4.jpeg')}  style={styles.child}/>
          </View>
        </SwiperFlatList>
        </View>
   
        )
    }
}
export const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    child: {
      height: height * 0.4,
      width,
      justifyContent: 'center',
      
    
    },
    text: {
      fontSize: width * 0.2,
      textAlign: 'center'
    }
  });
export default Swiper;