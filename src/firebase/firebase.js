import firebase from '@firebase/app'
import 'firebase/storage'

const config ={
    apiKey: "AIzaSyB0pU84L_50h7U7hnmETX6sMRDiNjgrN4A",
    authDomain: "uploadimages-94c6c.firebaseapp.com",
    databaseURL: "https://uploadimages-94c6c.firebaseio.com",
    projectId: "uploadimages-94c6c",
    storageBucket: "uploadimages-94c6c.appspot.com",
    messagingSenderId: "842752197464"
}

let instance = null ;

class FirebaseService {
    constructor(){
        if(!instance){
            this.app = firebase.initializeApp(config);
            instance = this;

        }
        return instance
    }
}
const firebaseService = new FirebaseService().app;

export default firebaseService