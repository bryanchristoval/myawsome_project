import React, { Component } from 'react'
import TapNavigator from './src/router/router'
import {StyleProvider} from 'native-base'
import material from './native-base-theme/variables/material'
import getTheme from './native-base-theme/components'
import RouterFlux from './src/router/routerFlux'

class App extends Component {
  render () {
    return (
      <StyleProvider style={getTheme(material)}>
        <RouterFlux/>
      </StyleProvider>
    )
  }
}

export default App